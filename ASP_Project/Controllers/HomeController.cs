﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using ASP_Project.Models;

using Microsoft.EntityFrameworkCore;
using System.Text.Json;
using System.Text;

namespace ASP_Project.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private Models.DataContext _db;

        public HomeController(ILogger<HomeController> logger, Models.DataContext db)
        {
            _logger = logger;
            _db = db;
        }

        public IActionResult Index()
        {
            return Redirect("~/home/getdata");
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult About()
        {
            ViewData["Message"] = "Your application description page.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Hello(string name)
        {
            ViewBag.Massenge = $"Hello {name}!";
            return View();
        }

        public IActionResult GetData()
        {
            //string json = System.IO.File.ReadAllText(@"data.txt");
            //_db.AddRange(JsonSerializer.Deserialize<List<Models.Person>>(json));

            //_db.SaveChanges();
            var people = _db.Persons.Include(c => c.name).OrderBy(a => a.index).ToList();

            return View(people);
        }
    }
}
