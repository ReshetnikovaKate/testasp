﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;

namespace ASP_Project
{
    public class TestController : Controller
    {
        // GET: /<controller>/
        private Services.IDateTimeHandler dateTime;
        public TestController (Services.IDateTimeHandler _dateTime)
        {
            dateTime = _dateTime;
      
        }
        public IActionResult Index()
        {
            return View();
        }

        public string GetString()
        {
            return "Hello";
        }
        public string Hello(String name)
        {
            return $"Hello, {name}!";
        }
        public string GetDayOfYear()
        {
            int DayOfYear = System.DateTime.Now.DayOfYear;
            return $"Today is {DayOfYear}th day of year!";
        }
        public string GetTime()
        {
            //int hours = System.DateTime.Now.Hour;
            //string massage;
            //if (hours <= 12 && hours > 6)
            //    massage = "time to wake up :(";
            //else if (hours <= 18 && hours > 12)
            //    massage = "adenture time!";
            //else if (hours <= 21 && hours > 18)
            //    massage = "time to rest!";
            //else
            //    massage = "time to sleep (:";
            //return $"Now {hours.ToString()} o'clock. It's {massage}";
            return dateTime.ConvertDateTimeToString(DateTime.Now);
        }
        public string Sum(int arg1, int arg2)
        {
            return $"{arg1} + {arg2} = {arg1 + arg2}";
        }
        public void Error()
        {
            //int b = 0;
            //int a = 3 / b;
        }
    }
}
