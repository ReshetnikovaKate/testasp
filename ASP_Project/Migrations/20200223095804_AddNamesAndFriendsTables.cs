﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ASP_Project.Migrations
{
    public partial class AddNamesAndFriendsTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Friend_People_Person_id",
                table: "Friend");

            migrationBuilder.DropForeignKey(
                name: "FK_People_Name_NameID",
                table: "People");

            migrationBuilder.DropPrimaryKey(
                name: "PK_People",
                table: "People");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Name",
                table: "Name");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Friend",
                table: "Friend");

            migrationBuilder.RenameTable(
                name: "People",
                newName: "Persons");

            migrationBuilder.RenameTable(
                name: "Name",
                newName: "Names");

            migrationBuilder.RenameTable(
                name: "Friend",
                newName: "Friends");

            migrationBuilder.RenameIndex(
                name: "IX_People_NameID",
                table: "Persons",
                newName: "IX_Persons_NameID");

            migrationBuilder.RenameIndex(
                name: "IX_Friend_Person_id",
                table: "Friends",
                newName: "IX_Friends_Person_id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Persons",
                table: "Persons",
                column: "_id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Names",
                table: "Names",
                column: "NameID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Friends",
                table: "Friends",
                column: "FriendID");

            migrationBuilder.AddForeignKey(
                name: "FK_Friends_Persons_Person_id",
                table: "Friends",
                column: "Person_id",
                principalTable: "Persons",
                principalColumn: "_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Persons_Names_NameID",
                table: "Persons",
                column: "NameID",
                principalTable: "Names",
                principalColumn: "NameID",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Friends_Persons_Person_id",
                table: "Friends");

            migrationBuilder.DropForeignKey(
                name: "FK_Persons_Names_NameID",
                table: "Persons");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Persons",
                table: "Persons");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Names",
                table: "Names");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Friends",
                table: "Friends");

            migrationBuilder.RenameTable(
                name: "Persons",
                newName: "People");

            migrationBuilder.RenameTable(
                name: "Names",
                newName: "Name");

            migrationBuilder.RenameTable(
                name: "Friends",
                newName: "Friend");

            migrationBuilder.RenameIndex(
                name: "IX_Persons_NameID",
                table: "People",
                newName: "IX_People_NameID");

            migrationBuilder.RenameIndex(
                name: "IX_Friends_Person_id",
                table: "Friend",
                newName: "IX_Friend_Person_id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_People",
                table: "People",
                column: "_id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Name",
                table: "Name",
                column: "NameID");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Friend",
                table: "Friend",
                column: "FriendID");

            migrationBuilder.AddForeignKey(
                name: "FK_Friend_People_Person_id",
                table: "Friend",
                column: "Person_id",
                principalTable: "People",
                principalColumn: "_id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_People_Name_NameID",
                table: "People",
                column: "NameID",
                principalTable: "Name",
                principalColumn: "NameID",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
