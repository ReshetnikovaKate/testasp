﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Project.Models
{
    public class Name
    { 
        [Key]
        public Guid NameID { get; set; }
        public string first { get; set; }
        public string last { get; set; }
    }

    public class Friend
    {
        [Key]
        public Guid FriendID { get; set; }
        public int id { get; set; }
        public string name { get; set; }
    }

    public class Person
    {
        [Key]
        public string _id { get; set; }
        public int index { get; set; }
        public string balance { get; set; }
        public string picture { get; set; }
        public int age { get; set; }
        public string eyeColor { get; set; }
        public Name name { get; set; }
        public string company { get; set; }
        public string email { get; set; }
        public string phone { get; set; }
        public string address { get; set; }
        public string about { get; set; }
        public List<string> tags { get; set; }
        public List<Friend> friends { get; set; }
        public string favoriteFruit { get; set; }
        public int CompareTo(Person p)
        {
            if (p == null)
                return 1;

            else
                return this.index.CompareTo(p.index);
        }
    }
}
