﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using System.Text.Json;

namespace ASP_Project
{
    public class PersonDict
    {
        private Dictionary<string, Models.Person> people;

        private static PersonDict instance;
        private PersonDict()
        {
            string json = System.IO.File.ReadAllText(@"data.txt");
            List<Models.Person> ListOfPeople = JsonSerializer.Deserialize<List<Models.Person>>(json);
            people = new Dictionary<string, Models.Person>();
            foreach (Models.Person p in ListOfPeople)
            {
                people.Add(p._id, p);
            }
        }
        public static PersonDict GetInstance()
        {
            if (instance == null)
                instance = new PersonDict();
            return instance;
        }
        private void Update()
        {
            string json = JsonSerializer.Serialize<List<Models.Person>>(people.Values.ToList<Models.Person>());
            System.IO.File.WriteAllText(@"Data.txt", json);
        }
        public void Delete(string id)
        {
            people.Remove(id);
            Update();
        }

        public void Add(Models.Person p)
        {
            people.Add(p._id, p);
            Update();
        }
        public void Edit()
        {

        }
    }
}
