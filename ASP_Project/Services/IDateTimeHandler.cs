﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Project.Services
{
    public interface IDateTimeHandler
    {
        string ConvertDateTimeToString(DateTime dateTime);
    }
}
