﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ASP_Project.Services
{
    public class UtsDateTimeHandler : IDateTimeHandler
    {
        public string ConvertDateTimeToString(DateTime dateTime)
        {
            return dateTime.ToUniversalTime().ToString("t") + " по UTC";
        }
    }
}
